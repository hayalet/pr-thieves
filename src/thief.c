#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include <algorithm>

/* Message types */

#define MSG_REQUEST 0
#define MSG_APPROVE 1 
#define MSG_RELEASE 2

/* Packet Datatype */

MPI_Datatype MPI_PACKET;

typedef struct packet_t {
	int l_clock;
	int camels_req;
} packet;

/* Thief queue */

struct thief {
	int rank;
	int l_clock;
	int camels_req;
};

bool thief_cmp(const thief &a, const thief &b)
{
	if (a.l_clock < b.l_clock) {
		return true;
	} else if (a.l_clock == b.l_clock && a.rank < b.rank) {
		return true;
	}

	return false;
}

/* Process variables */

#define MAX_WAIT_TIME 50000 // 5 secs - MAX_WAIT_TIME * 0.0001 (100 msecs) = Wait time in secs

int rank, size;

bool require_camels = false;
bool on_robbery = false;
bool waiting_for_camels = false;

int chests_required = 0;
int camels_required = 0;

int logical_clock = 0;
int request_time = 0;
int camels_count = 0;
int max_camels = 0;
int wait_time = 0;

std::vector<bool> approvals;
std::vector<thief> waiting_thieves;

/////////////////////////////////////////////////////////////////////////////////////////
// MPI Wrappers
/////////////////////////////////////////////////////////////////////////////////////////

void init_datatype()
{
	const int nitems = 2;
	int blocklengths[2] = {1,1};
	MPI_Datatype types[2] = {MPI_INT, MPI_INT};
	MPI_Aint offsets[2];

	offsets[0] = offsetof(packet, l_clock);
	offsets[1] = offsetof(packet, camels_req);

	MPI_Type_create_struct(nitems, blocklengths, offsets, types, &MPI_PACKET);
	MPI_Type_commit(&MPI_PACKET);
}

void finish()
{
	MPI_Type_free(&MPI_PACKET);
	MPI_Finalize();
}

void receive(MPI_Status &status, packet &msg)
{
	MPI_Recv(&msg, 1, MPI_PACKET, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	logical_clock = std::max(logical_clock, msg.l_clock) + 1;
}

void send(int dest, int tag)
{
	packet msg;
	msg.l_clock = logical_clock + 1;
	msg.camels_req = camels_required;

	MPI_Send(&msg, 1, MPI_PACKET, dest, tag, MPI_COMM_WORLD);
}

void send_to_all(int tag)
{
	for (int dest = 0; dest < size; dest++) {
		if (dest == rank) continue;
		send(dest, tag);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
// Process logic
/////////////////////////////////////////////////////////////////////////////////////////

void go_on_robbery()
{
	logical_clock += 1;

	chests_required = rand() % 10 + 1; // Maximum 10 chests
	camels_required = std::min((chests_required + 2 - 1) / 2, max_camels); // Every camel can carry maximum 2 chests

	require_camels = true;
	request_time = logical_clock;

	send_to_all(MSG_REQUEST);

	printf("%d %d \e[1;34mREQUEST %d CAMELS \e[0m\n", rank, logical_clock, camels_required, request_time);
}

bool is_sufficient_camels(int count = 0)
{
	if (camels_count >= camels_required + count) {	
		return true;
	} 

	return false;
}

void take_camels() 
{
	logical_clock += 1;

	camels_count -= camels_required;
	on_robbery = true;
	waiting_for_camels = false;
	require_camels = false;

	printf("%d %d \e[1;31mWENT ON ROBBERY [C: %d/%d] \e[0m\n", rank, logical_clock, camels_count, max_camels);

	if (waiting_thieves.size() > 0) {
		for(int i = 0; i < waiting_thieves.size(); i++) {
			camels_count -= waiting_thieves[i].camels_req;
			send(waiting_thieves[i].rank, MSG_APPROVE);

			#ifdef DEBUG
				printf("%d %d SEND APPROVE TO %d\n", rank, logical_clock, waiting_thieves[i].rank);
			#endif
		}
		waiting_thieves.clear();
	}

	wait_time = rand() % MAX_WAIT_TIME;
}

void return_from_robbery()
{
	logical_clock += 1;

	on_robbery = false;
	send_to_all(MSG_RELEASE);
	camels_count += camels_required;

	printf("%d %d \e[1;33mRELEASED %d CAMELS \e[0m\n", rank, logical_clock, camels_required);

	camels_required = 0;
	chests_required = 0;

	approvals.clear();
	approvals.resize(size, false);

	wait_time = rand() % MAX_WAIT_TIME;
}

bool is_having_priority(packet msg, int source)
{
	if (request_time < msg.l_clock) {
		return true;
	} else if (request_time == msg.l_clock && rank < source) {
		return true;
	}

	return false;
}

bool is_approved_by_all()
{
	for (int i = 0; i < approvals.size(); i++) {
		if (i == rank) continue;
		if (approvals[i] == false) {
			return false;
		}
	}

	return true;
}

void on_request(packet msg, int source)
{
	if (require_camels) {
		if (is_having_priority(msg, source)) {
			if (is_sufficient_camels(msg.camels_req)) {
				send(source, MSG_APPROVE);	
				camels_count -= msg.camels_req;

				#ifdef DEBUG
					printf("%d %d SEND APPROVE TO %d\n", rank, logical_clock, source);
				#endif
			} else {
				thief next;
				next.rank = source;
				next.l_clock = msg.l_clock;
				next.camels_req = msg.camels_req;
				waiting_thieves.push_back(next);

				#ifdef DEBUG
					printf("%d %d ADD %d TO QUEUE\n", rank, logical_clock, source);
				#endif
			}
		} else {
			send(source, MSG_APPROVE);	
			camels_count -= msg.camels_req;	

			#ifdef DEBUG
				printf("%d %d SEND APPROVE TO %d\n", rank, logical_clock, source);
			#endif
		}
	} else {
		send(source, MSG_APPROVE);
		camels_count -= msg.camels_req;	

		#ifdef DEBUG
			printf("%d %d SEND APPROVE TO %d\n", rank, logical_clock, source);
		#endif
	}
}

void on_approve(int source)
{
	approvals[source] = true;

	if (is_approved_by_all()) {
		waiting_for_camels = true;
		if (is_sufficient_camels()) {
			take_camels();	
		}
	}
}

void on_release(packet msg)
{
	camels_count += msg.camels_req;
}

void receive_message()
{
	MPI_Status status;
	packet msg;

	receive(status, msg);

	switch (status.MPI_TAG) {
		case MSG_REQUEST:
			on_request(msg, status.MPI_SOURCE);

			#ifdef DEBUG
				printf("%d %d RECEIVED REQUEST %d CAMELS FROM %d\n", rank, logical_clock, msg.camels_req, status.MPI_SOURCE);
			#endif

			break;
		case MSG_APPROVE:
			on_approve(status.MPI_SOURCE);

			#ifdef DEBUG
				printf("%d %d RECEIVED APPROVE FROM %d\n", rank, logical_clock, status.MPI_SOURCE);
			#endif

			break;
		case MSG_RELEASE:
			on_release(msg);
			
			#ifdef DEBUG
				printf("%d %d RECEIVED RELEASE %d CAMELS FROM %d\n", rank, logical_clock, msg.camels_req, status.MPI_SOURCE);
			#endif

			break;
	}
}

void check_for_messages() 
{
	int flag = 0;

	MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, MPI_STATUS_IGNORE);

	if (flag) {
		receive_message();
	}
}

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	init_datatype();

	if (argc < 2) {
		finish();
		return EXIT_FAILURE;
	}

	max_camels = atoi(argv[1]);
	camels_count = max_camels;
	approvals.resize(size, false);

	srand(time(NULL) + rank);
	wait_time = rand() % MAX_WAIT_TIME;

	while (1) {
		if (wait_time == 0) {
			if (!on_robbery) {
				go_on_robbery();
			} else {
				return_from_robbery();
			}
		}
		
		if (waiting_for_camels && is_sufficient_camels()) {
			take_camels();
		}

		check_for_messages();

		if (wait_time >= 0) 
			wait_time--;

		usleep(100);
	}

	finish();
	return EXIT_SUCCESS;
}