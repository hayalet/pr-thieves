PROCS=15
CAMELS=10

clean:
	@rm -rf bin
	@mkdir bin

release:
	@mpiCC src/thief.c -o bin/thief

debug: 
	@mpiCC -DDEBUG src/thief.c -o bin/thief

run:
	@mpirun -np $(PROCS) bin/thief $(CAMELS)