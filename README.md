# Rozbójnicy alibaby

Projekt na przedmiot Programowanie Rozproszone.

### Treść zadania

Alibaba, po osiągnięciu wieku emerytalnego, rezyduje w Sezamie i, nie opuszczając go, gromadzi łupy nieustannie dostarczane przez swoją rozproszoną bandę, która obejmuje R okrutnych rozbójników i W jucznych wielbłądów (W < R). Rozbójnicy szmuglują do Sezamu od 1 do 10 skrzyni z pozyskanymi w drodze rabunku precjozjami. Transport umożliwiają wielbłądy, z których każdy zdoła unieść nie więcej niż 2 skrzynie. Napisać program dla procesu rozbójnika. Wielbłądy należy traktować jako zasoby. Sezam posiada nieskończoną pojemność. Rabunki odbywają się spontanicznie. 

### Instalacja MPI

**Debian/Ubuntu**

`sudo apt-get install openmpi-bin openmpi-doc libopenmpi-dev`